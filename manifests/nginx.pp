# Sets up nginx and maintains the service
class tazserve::nginx {
  package { 'nginx':
    ensure => installed,
  }

  file { '/etc/nginx/nginx.conf':
    ensure  => present,
    source  => 'puppet:///modules/tazserve/nginx/nginx.conf',
    require => Package['nginx'],
    notify  => Service['nginx'],
  }

  file { '/etc/nginx/sites-enabled':
    ensure  => directory,
    require => Package['nginx'],
  }

  file { '/etc/nginx/sites-enabled/tazblog':
    ensure => present,
    source => 'puppet:///modules/tazserve/nginx/tazblog',
    notify => Service['nginx'],
  }

  file { '/etc/nginx/sites-enabled/democrify':
    ensure => present,
    source => 'puppet:///modules/tazserve/nginx/tazblog',
    notify => Service['nginx'],
  }

  service { 'nginx':
    ensure => running,
    enable => true,
  }
}
