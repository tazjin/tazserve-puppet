# Adds a single user

define tazserve::user(
  $username = $name,
  $uid
  ) {

  user { $username:
    ensure         => present,
    home           => "/home/${username}",
    purge_ssh_keys => true,
    managehome     => true,
    groups         => ['wheel'],
    shell          => '/usr/bin/fish',
    uid            => $uid,
    gid            => $username,
  }

  # managehome doesn't work on ArchLinux apparently
  file { "/home/${username}":
    ensure => directory,
    owner  => $username,
    group  => $username,
  }

  group { $username:
    gid => $uid,
  }

  ssh_authorized_key { $username:
    ensure => present,
    key    => template("tazserve/${username}-key"),
    type   => 'ssh-rsa',
    user   => $username,
  }  
}
