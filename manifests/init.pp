# Umbrella class
class tazserve {
  include tazserve::nginx
  include tazserve::puppet
  include tazserve::system
  include tazserve::tazblog
}
