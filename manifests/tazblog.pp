# Ensures that the tazblog package is installed, sets
# permissions accordingly and maintains the service.
class tazserve::tazblog {
  package { 'tazblog':
    ensure => installed,
    # Will cause a failure if the package isn't installed
    # because it's not in any repo / AUR
  }

  file { '/var/tazblog':
    ensure  => directory,
    owner   => 'vincent',
    recurse => true,
  }

  service { 'tazblog@vincent':
    ensure  => running,
    enable  => true,
    require => [Package['tazblog'], File['/var/tazblog']],
  }
}
