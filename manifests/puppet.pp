# Puppet setup
class tazserve::puppet {
  file {'/usr/bin/tpuppet':
    ensure => present,
    source => 'puppet:///modules/tazserve/tpuppet',
    mode   => '0755',
  }

  file {'/etc/puppet/manifests/':
    ensure => directory,
  }

  file {'/etc/puppet/manifests/init.pp':
    ensure => present,
    source => 'puppet:///modules/tazserve/init.pp',
  }

  cron { 'puppet':
    command => '/usr/bin/tpuppet',
    user    => root,
    minute  => [20, 50],
  }
}
