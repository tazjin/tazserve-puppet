# Generic system configuration and tools
class tazserve::system {
  $packages = ['tree', 'pacmatic', 'emacs-nox', 'iftop', 'openssh']

  package { $packages:
    ensure => installed,
  }

  # We only want emacs-nox
  package { 'emacs':
    ensure => absent,
  }

  service { 'cronie':
    ensure => running,
    enable => true,
  }

  # Network configuration through systemd-networkd
  file { '/etc/systemd/network/50-static.network':
    ensure => present,
    owner  => 'root',
    group  => 'root',
    source => 'puppet:///modules/tazserve/50-static.network',
  }

  file { '/etc/resolv.conf':
    ensure => present,
    source => 'puppet:///modules/tazserve/resolv.conf',
  }

  service { 'systemd-networkd':
    ensure => running,
    enable => true,
  }

  # SSHd configuration
  file { '/etc/ssh/sshd_config':
    ensure  => present,
    source  => 'puppet:///modules/tazserve/sshd_config',
    require => Package['openssh'],
    notify  => Service['sshd'],
  }

  service { 'sshd':
    ensure => running,
  }

  tazserve::user { 'vincent':
    uid => 1000,
  }

  tazserve::user { 'davw':
    uid => 1200,
  }
}
